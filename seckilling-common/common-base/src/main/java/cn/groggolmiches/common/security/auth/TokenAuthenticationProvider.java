package cn.groggolmiches.common.security.auth;

import cn.groggolmiches.common.dto.SecurityUserDTO;
import cn.groggolmiches.common.util.SecurityUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * 对认证过程的二次处理
 * @author zhanhong zhang
 * Created on 2020/11/13
 */
@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        SecurityUserDTO tokenDTO = (SecurityUserDTO) authentication.getPrincipal();

        //去redis里找之前存放的信息
        SecurityUserDTO user =  SecurityUtils.getUser(tokenDTO.getId());
        if (user != null) {
            long timestamp = user.getTimestamp();
            if (timestamp == tokenDTO.getTimestamp()) {
                return new UsernamePasswordAuthenticationToken(user, "", user.getAuthorities());
            }
        }
        throw new CredentialsExpiredException("登录失效，请重新登录");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
