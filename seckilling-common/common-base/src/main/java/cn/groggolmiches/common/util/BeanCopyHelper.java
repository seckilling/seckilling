package cn.groggolmiches.common.util;

import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @description: Bean转换工具
 * @author: zhanhong zhang
 * @date: 2020/11/7 19:07
 */
public class BeanCopyHelper {

    /**
     * Bean转换静态工具方法
     * @param source：源对象
     * @param clazz：需要转换的类型Class
     * @return 转换后的对象
     */
    public static <T> T copyProperties(Object source, Class<T> clazz) {
        if (null == source) {
            return null;
        } else {
            try {
                T obj = clazz.newInstance();
                BeanUtils.copyProperties(source, obj);
                return obj;
            } catch (IllegalAccessException | InstantiationException var3) {
                throw new RuntimeException(var3);
            }
        }
    }

    /**
     * 集合转换静态工具方法
     * @param source：源集合
     * @param clazz：需要转换的类型Class
     * @return 返回转换后的集合
     */
    public static <T> List<T> copyCollection(Collection source, Class<T> clazz) {
        if (null == source) {
            return new ArrayList();
        } else {
            List<T> list = new ArrayList();
            Iterator iterator = source.iterator();

            while (iterator.hasNext()) {
                Object o = iterator.next();
                list.add(copyProperties(o, clazz));
            }
            return list;
        }
    }

}
