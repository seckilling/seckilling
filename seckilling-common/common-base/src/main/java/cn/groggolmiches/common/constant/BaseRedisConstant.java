package cn.groggolmiches.common.constant;

import java.text.MessageFormat;

/**
 * 基础redis缓存键常量类
 * @author zhang zhanhong
 * @date 2020/11/13
 */
public class BaseRedisConstant {

    public static String SECURITY_LOGIN_TOKEN = "security:login:token:{0}";

    public static String getSecurityLoginToken(Long id){
        return MessageFormat.format(SECURITY_LOGIN_TOKEN,id);
    }

}
