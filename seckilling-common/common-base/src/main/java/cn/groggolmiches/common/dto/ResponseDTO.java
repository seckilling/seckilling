package cn.groggolmiches.common.dto;

import cn.groggolmiches.common.enums.BaseResponseEnums;
import lombok.Data;

import java.io.Serializable;

/**
 * 基础响应DTO
 *
 * @author zhang zhanhong
 * @date 2020/10/29
 */
@Data
public class ResponseDTO<T> implements Serializable {
    private static final long serialVersionUID = 3425543151011600581L;
    /**
     * 响应编码
     */
    private Integer code;
    /**
     * 响应消息
     */
    private String msg;
    /**
     * 响应数据
     */
    private T data;

    /**
     * 全参构造器
     *
     * @param code：响应编码
     * @param msg：响应消息
     * @param data：响应数据
     */
    public ResponseDTO(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 两参构造器
     *
     * @param code：响应编码
     * @param msg：响应消息
     */
    public ResponseDTO(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 无参构造器
     */
    public ResponseDTO() {
    }

    /**
     * 静态方法--请求成功默认响应
     *
     * @return ResponseDTO
     */
    public static ResponseDTO success() {
        return new ResponseDTO(BaseResponseEnums.REQUEST_SUCCESS.getCode(), BaseResponseEnums.REQUEST_SUCCESS.getMsg(), null);
    }

    /**
     * 静态方法--请求成功默认带数据响应
     *
     * @param data：返回响应数据
     * @param <T>：数据类型
     * @return ResponseDTO
     */
    public static <T> ResponseDTO success(T data) {
        return new ResponseDTO(BaseResponseEnums.REQUEST_SUCCESS.getCode(), BaseResponseEnums.REQUEST_SUCCESS.getMsg(), data);
    }

    /**
     * 静态方法--请求失败默认响应
     *
     * @return ResponseDTO
     */
    public static ResponseDTO fail() {
        return new ResponseDTO(BaseResponseEnums.REQUEST_FAIL.getCode(), BaseResponseEnums.REQUEST_FAIL.getMsg(), null);
    }

    /**
     * 自定义响应信息
     * @param code：响应编码
     * @param msg：响应信息
     * @return 响应信息
     */
    public static ResponseDTO response(Integer code, String msg) {
        return new ResponseDTO(code, msg);
    }

}
