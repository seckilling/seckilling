package cn.groggolmiches.common.constant;

/**
 * 基础常量类
 *
 * @author zhang zhanhong
 * @date 2020/10/29
 */
public class BaseConstant {

    /**
     * 默认请求成功编码
     */
    public final static Integer REQUEST_SUCCESS_CODE = 200;
    /**
     * sso单点登录的header
     */
    public static final String SSO_TOKEN_HEADER = "X-Requested-Token";

    /**
     * 请求头带有X-Feign代表内部调用，直接放行
     */
    public  static  final  String X_FEIGN = "X-Feign";
}
