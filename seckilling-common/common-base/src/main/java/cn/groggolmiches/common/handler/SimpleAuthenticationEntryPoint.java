package cn.groggolmiches.common.handler;
import cn.groggolmiches.common.enums.BaseResponseEnums;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * @author zhanhong zhang
 * @Date 2020/11/24
 * 处理认证异常AuthenticationException
 */
@Component
public class SimpleAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        //未登录
        boolean b = authException instanceof InsufficientAuthenticationException;
        HashMap<String, Object> map = new HashMap<>(4);
        map.put("code", b ? BaseResponseEnums.NO_LOGIN.getCode() : BaseResponseEnums.LOGIN_REPLACED.getCode());
        map.put("msg", b ? BaseResponseEnums.NO_LOGIN.getMsg() : authException.getMessage());
        response.setCharacterEncoding("utf-8");
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        ObjectMapper objectMapper = new ObjectMapper();
        String resBody = objectMapper.writeValueAsString(map);
        PrintWriter printWriter = response.getWriter();
        printWriter.print(resBody);
        printWriter.flush();
        printWriter.close();
    }
}