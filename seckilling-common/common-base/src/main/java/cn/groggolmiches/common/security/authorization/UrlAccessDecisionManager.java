package cn.groggolmiches.common.security.authorization;

import cn.groggolmiches.common.constant.BaseConstant;
import cn.groggolmiches.common.dto.SecurityUserDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**自定义拦截url信息处理
 * @author xrt
 * @date 2020/9/18 19:25
 */
@Component
public class UrlAccessDecisionManager implements AccessDecisionManager {


    /**
     * @param authentication: 当前登录用户的角色信息
     * @param object:         请求url信息
     * @param collection:     `UrlFilterInvocationSecurityMetadataSource`中的getAttributes方法传来的，表示当前请求需要的角色（可能有多个）
     */
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> collection) throws AccessDeniedException, AuthenticationException {
        HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
        //请求头带有X-Feign代表内部调用，直接放行
        if (StringUtils.isNotBlank(request.getHeader(BaseConstant.X_FEIGN))){
            return;
        }

        if (authentication instanceof AnonymousAuthenticationToken)
        {
            throw new InsufficientAuthenticationException("未登录，请先登录");

        }

        String requestUri = request.getRequestURI();
        SecurityUserDTO securityUserDTO = (SecurityUserDTO) authentication.getPrincipal();
        AntPathMatcher matcher = new AntPathMatcher();
        for (GrantedAuthority url : securityUserDTO.getAuthorities()) {
            if (matcher.match(url.getAuthority(), requestUri) || url.getAuthority().equals("ROLE_admin"))
                return;
        }
        //TODO 后面再做
        throw new AccessDeniedException("没有权限，请联系管理员分配权限！");
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}