package cn.groggolmiches.common.util;

import cn.groggolmiches.common.constant.BaseRedisConstant;
import cn.groggolmiches.common.dto.SecurityUserDTO;
import cn.groggolmiches.common.enums.BaseResponseEnums;
import cn.groggolmiches.common.exception.BaseException;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * 权限工具
 *
 * @author zhang zhanhong
 * @date 2020/11/13
 */
@Component
public class SecurityUtils {

    @Autowired
    private RedisTemplate redisTemplate;

    private static RedisTemplate redis;

    @PostConstruct
    public void setRedis() {
        redis = redisTemplate;
    }

    /**
     * 获取用户信息
     */
    public static SecurityUserDTO getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            throw new BaseException(BaseResponseEnums.NO_LOGIN);
        }
        if (authentication == null) {
            throw new BaseException(BaseResponseEnums.NO_LOGIN);
        }
        return (SecurityUserDTO) authentication.getPrincipal();
    }

    /**
     * 查询是否有该用户id对应的用户信息
     *
     * @return 用户token
     */
    public static SecurityUserDTO getUser(Long id) {
        // 获取token的redis缓存key
        String key = BaseRedisConstant.getSecurityLoginToken(id);

        SecurityUserDTO securityUserDTO = null;
        // 获取对应token
        String token = (String) redis.opsForValue().get(key);
        if (!StringUtils.isEmpty(token)) {
            // 使用Base64将token转换成json字符串，再转成SecurityUserDTO
            String json = new String(Base64Utils.decodeFromString(token));
            securityUserDTO = JSONObject.parseObject(json, SecurityUserDTO.class);
        }
        return securityUserDTO;
    }

    /**
     * 将token加入redis缓存
     *
     * @param id：用户id
     * @param token：token
     */
    public static void setUser(Long id, String token) {
        // 将token放入redis缓存，3小时过期时间
        redis.opsForValue().set(BaseRedisConstant.getSecurityLoginToken(id), token,3, TimeUnit.HOURS);
    }

    /**
     * 获取登录用户的id
     *
     * @return 用户id
     */
    public static Long getUserId() {
        SecurityUserDTO user = getUser();
        return user.getId();
    }

}
