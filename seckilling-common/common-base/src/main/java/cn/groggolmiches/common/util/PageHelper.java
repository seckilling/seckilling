package cn.groggolmiches.common.util;

import cn.groggolmiches.common.dto.PageDTO;
import cn.groggolmiches.common.enums.BaseResponseEnums;
import cn.groggolmiches.common.exception.BaseException;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 分页插件
 * @author: zhanhong zhang
 * @date: 2020/11/7 19:26
 */
@Data
public class PageHelper {

    /**
     * 获取分页信息的静态工具方法
     *
     * @param source：需要分页的数据
     * @param pageNum：页数
     * @param pageSize：页码
     * @return 返回分页数据
     */
    public static <T> PageDTO page(List<T> source, Integer pageNum, Integer pageSize) {
        /**
         * 构建分页数据
         */
        // 分页数据
        List<T> data;
        // 是否首页
        boolean isFirst = false;
        // 是否尾页
        boolean isLast = false;
        // 上一页
        Integer prePage = null;
        // 下一页
        Integer nextPage = null;
        // 获取数据总数
        int total = source.size();
        // 数据起始下标
        int fromIndex = (pageNum - 1) * pageSize;
        // 数据结束下标
        int toIndex = fromIndex + pageSize;

        if (total == 0) {
            data = new ArrayList<>();
        } else if (fromIndex < total && toIndex >= total) {
            // 该页数据不足一页
            data = source.subList(fromIndex, total);
            isLast = true;
            isFirst = false;

            if (pageNum != 1) {
                prePage = pageNum - 1;
                nextPage = null;
            }
        } else if (fromIndex < total && toIndex < total) {
            data = source.subList(fromIndex, toIndex);
            if (pageNum == 1) {
                prePage = null;
                nextPage = pageNum + 1;
            } else {
                prePage = pageNum - 1;
                nextPage = pageNum + 1;
            }
        } else {
            throw new BaseException(BaseResponseEnums.REQUEST_FAIL);
        }

        if (pageNum.equals(1)) {
            isFirst = true;
        }

        return new PageDTO(total, pageNum, pageSize, prePage, nextPage, isFirst, isLast, data);
    }

}
