package cn.groggolmiches.common.exception;

import cn.groggolmiches.common.enums.BaseResponseEnums;
import lombok.Data;

/**
 * 基础异常类，向上抛出运行时异常
 *
 * @author zhang zhanhong
 * @date 2020/10/29
 */
@Data
public class BaseException extends RuntimeException {
    private Integer code;

    private String msg;

    public BaseException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseException(BaseResponseEnums baseResponseEnums) {
        this(baseResponseEnums.getCode(), baseResponseEnums.getMsg());
    }
}
