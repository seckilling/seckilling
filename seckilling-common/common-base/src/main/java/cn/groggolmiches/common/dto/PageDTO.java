package cn.groggolmiches.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: 分页DTO
 * @author: zhanhong zhang
 * @date: 2020/11/7 19:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageDTO<T> {
    /**
     * 总数
     */
    private Integer total;
    /**
     * 当前页数
     */
    private Integer pageNum;
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 上一页数
     */
    private Integer prePage;
    /**
     * 下一页数
     */
    private Integer nextPage;
    /**
     * 是否首页
     */
    private Boolean isFirstPage;
    /**
     * 是否尾页
     */
    private Boolean isLastPage;
    /**
     * 数据
     */
    private List<T> data;

}
