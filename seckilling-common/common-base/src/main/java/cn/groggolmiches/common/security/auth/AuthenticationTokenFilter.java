package cn.groggolmiches.common.security.auth;

import cn.groggolmiches.common.constant.BaseConstant;
import cn.groggolmiches.common.dto.SecurityUserDTO;
import cn.groggolmiches.common.util.SecurityUtils;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用于拦截所有请求，进行解析认证
 *
 * @author xrt
 * Created on 2020/9/17
 */
@Component
public class AuthenticationTokenFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        Authentication authentication = checkAuthentication(request);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    /**
     * 校验token信息，如果解析成功，交由TokenAuthenticationProvider处理
     */
    private Authentication checkAuthentication(HttpServletRequest request) {
        //解析请求头.token放在请求头做管理
        String token = request.getHeader(BaseConstant.SSO_TOKEN_HEADER);
        if (StringUtils.isEmpty(token)) {
            return null;
        }
        String securityUserJson = new String(Base64Utils.decodeFromString(token));
        if (StringUtils.isEmpty(securityUserJson)) {
            return null;
        }
        // 使用Base64将token转换成json字符串，再转成SecurityUserDTO
        SecurityUserDTO tokenDTO = JSONObject.parseObject(securityUserJson, SecurityUserDTO.class);
        SecurityUserDTO user = SecurityUtils.getUser(tokenDTO.getId());

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, null);
        return authenticationToken;
    }
}