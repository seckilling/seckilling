package cn.groggolmiches.common.util;

import cn.groggolmiches.common.enums.BaseResponseEnums;
import cn.groggolmiches.common.exception.BaseException;

/**
 * @description: 参数检查工具
 * @author: zhanhong zhang
 * @date: 2020/11/7 20:42
 */
public class NecessaryPropertiesCheckHelper {

    /**
     * 参数检查
     * @param properties：参数列表
     */
    public static void checkNotNull(Object ... properties){
        for (Object o:properties) {
            if (o == null){
                throw new BaseException(BaseResponseEnums.NECESSARY_PROPERTIES_IS_NULL);
            }
        }
    }
}
