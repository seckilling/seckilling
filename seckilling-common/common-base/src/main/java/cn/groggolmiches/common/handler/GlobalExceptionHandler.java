package cn.groggolmiches.common.handler;

import cn.groggolmiches.common.dto.ResponseDTO;
import cn.groggolmiches.common.enums.BaseResponseEnums;
import cn.groggolmiches.common.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhang zhanhong
 * @description: 全局异常拦截
 * @date 2020/10/29
 */
@ControllerAdvice
@RestController
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 处理基础异常
     *
     * @param e：基础异常
     * @return ResponseDTO
     */
    @ExceptionHandler(value = BaseException.class)
    public ResponseDTO baseExceptionHandler(BaseException e) {
        log.error(e.getMessage());
        return new ResponseDTO(e.getCode(), e.getMsg());
    }

    /**
     * 处理java异常
     *
     * @param e：异常
     * @return 异常信息
     */
    @ExceptionHandler(value = Exception.class)
    public ResponseDTO exceptionHandler(Exception e) {
        log.error("",e);
        return new ResponseDTO(BaseResponseEnums.REQUEST_FAIL.getCode(), BaseResponseEnums.REQUEST_FAIL.getMsg());
    }

}