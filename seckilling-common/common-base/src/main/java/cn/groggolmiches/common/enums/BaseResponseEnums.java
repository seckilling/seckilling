package cn.groggolmiches.common.enums;

/**
 * 基础响应枚举
 *
 * @author zhang zhanhong
 * @date 2020/10/29
 */
public enum BaseResponseEnums {

    REQUEST_SUCCESS(200, "请求成功"),
    NO_LOGIN(201, "未登录"),
    LOGIN_REPLACED(202, "您的账号在别处登录！"),
    REQUEST_FAIL(500, "服务器错误，请联系客服解决"),
    NECESSARY_PROPERTIES_IS_NULL(10000,"核心参数不能为空");

    private Integer code;

    private String msg;

    BaseResponseEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    BaseResponseEnums() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
