package cn.groggolmiches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 公共网关启动类
 * @author zhanhong zhang
 * @date 2020/11/13
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CommonGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommonGatewayApplication.class, args);
	}

}
