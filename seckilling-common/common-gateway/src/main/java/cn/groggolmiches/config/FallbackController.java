package cn.groggolmiches.config;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 网关Fallback
 *
 * @author zhanhong zhang
 * @date 2020/11/25
 */
@RestController
public class FallbackController {

    @GetMapping("/fallback")
    public Object fallback() {
        Map<String,Object> result = new HashMap<>(4);
        result.put("data",null);
        result.put("msg","Get Request But Gateway Fallback!");
        result.put("code",501);
        return result;
    }
}
