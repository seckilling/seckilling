package cn.groggolmiches.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.NettyWriteResponseFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 网关全局过滤器
 * @author zhanhong zhang
 * @date 2020/11/25
 */
@Component
public class WebAppFilter implements GlobalFilter, Ordered {
    private static final Logger log = LoggerFactory.getLogger(WebAppFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("-----------------Gateway---------------------");
        log.info(" 前置 : " +exchange.getRequest().getBody() + "\t"+ exchange.getRequest().getURI().toString());
        String token = exchange.getRequest().getHeaders().getFirst("X-Requested-Token");
        log.info("token------->" + token);

        if (token !=null){
            ServerHttpRequest host = exchange.getRequest().mutate().headers(httpHeaders -> {
                httpHeaders.add("X-Requested-Token", token);
            }).build();

            ServerWebExchange build = exchange.mutate().request(host).build();
            return chain.filter(build).then(Mono.fromRunnable(()-> {
                log.info(" 后置 : " + exchange.getResponse().getStatusCode() + "\t" + exchange.getRequest().getURI().toString());
            }));

        }

        return chain.filter(exchange);
    }


    @Override
    public int getOrder() {
        return NettyWriteResponseFilter.WRITE_RESPONSE_FILTER_ORDER - 1;
    }
}
