package cn.groggolmiches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 商品服务启动类
 * @author zhanhong zhang
 * @date 2020/11/3
 */
@SpringBootApplication
public class ServerCommodityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerCommodityApplication.class, args);
	}

}
