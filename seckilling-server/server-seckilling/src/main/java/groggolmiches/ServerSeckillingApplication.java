package groggolmiches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 秒杀服务启动类
 *
 * @author zhanhong zhang
 * @date 2020/10/30
 */
@SpringBootApplication
public class ServerSeckillingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerSeckillingApplication.class, args);
	}

}
