package cn.groggolmiches.entity;

import lombok.Data;

import javax.persistence.Id;
import java.util.Date;

/**
 * @description: 用户信息实体类
 * @author: zhanhong zhang
 * @date: 2020/11/7 18:38
 */
@Data
public class UserInfo {
    /**
     * 用户信息表主键id
     */
    @Id
    private Long id;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 加密密码
     */
    private String password;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
}
