package cn.groggolmiches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 用户服务启动类
 *
 * @author zhanhong zhang
 * @date 2020/10/30
 */
@SpringBootApplication
@MapperScan("cn.groggolmiches.mapper")
@EnableDiscoveryClient
public class ServerUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerUserApplication.class, args);
    }

}
