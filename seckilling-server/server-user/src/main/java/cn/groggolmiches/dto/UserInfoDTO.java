package cn.groggolmiches.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @description: 用户信息DTO
 * @author: zhanhong zhang
 * @date: 2020/11/7 18:38
 */
@Data
public class UserInfoDTO {

    @ApiModelProperty("用户信息表主键id")
    private Long id;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("加密密码")
    private String password;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("修改时间")
    private Date gmtModified;

}
