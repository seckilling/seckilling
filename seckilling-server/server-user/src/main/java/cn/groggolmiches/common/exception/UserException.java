package cn.groggolmiches.common.exception;

import cn.groggolmiches.common.enums.UserEnums;

/**
 * @description: 用户服务专用异常类
 * @author: zhanhong zhang
 * @date: 2020/11/7 20:47
 */
public class UserException extends BaseException {

    /**
     * 用户异常类构造器
     * @param code：异常编码
     * @param msg：异常信息
     */
    public UserException(Integer code, String msg) {
        super(code, msg);
    }

    /**
     * 用户异常类构造器
     * @param userEnums：用户服务专用枚举
     */
    public UserException(UserEnums userEnums) {
        super(userEnums.getCode(), userEnums.getMsg());
    }
}
