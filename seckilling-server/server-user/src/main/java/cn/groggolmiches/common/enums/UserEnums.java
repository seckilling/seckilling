package cn.groggolmiches.common.enums;

/**
 * @description:
 * @author: zhanhong zhang
 * @date: 2020/11/7 20:49
 */
public enum UserEnums {

    REGISTER_FAIL(10001,"注册失败"),
    WORONG_ACCOUNT_OR_PASSWORD(10002,"用户名或密码错误"),
    ILLEGAL_REQUEST(10003,"非法请求"),
    ACCOUNT_EXISTS(10004,"用户名已存在");

    private Integer code;

    private String msg;

    UserEnums() {
    }

    UserEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }


    public String getMsg() {
        return msg;
    }

}
