package cn.groggolmiches.common.constant;

import java.text.MessageFormat;

/**
 * @description: 用户redis键常量
 * @author: zhanhong zhang
 * @date: 2020/11/7 22:13
 */
public class UserRedisConstant {
    /**
     * 用户密钥键
     */
    private static final String USER_PRIVATE_KEY = "user:privateKey:{0}";

    /**
     * 根据手机号获取对应密钥
     * @param phone：用户名
     * @return 密钥
     */
    public static String getUserPrivateKey(String phone){
        return MessageFormat.format(USER_PRIVATE_KEY,phone);
    }

}
