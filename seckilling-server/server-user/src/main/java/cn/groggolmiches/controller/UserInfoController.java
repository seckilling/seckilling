package cn.groggolmiches.controller;

import cn.groggolmiches.common.dto.ResponseDTO;
import cn.groggolmiches.dto.UserInfoDTO;
import cn.groggolmiches.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description: 用户信息控制层
 * @author: zhanhong zhang
 * @date: 2020/11/7 20:04
 */
@Api(value = "用户信息", tags = "用户信息")
@RestController
@RequestMapping("login")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @ApiOperation(value = "注册", notes = "注册")
    @PostMapping("register")
    public ResponseDTO register(@RequestBody UserInfoDTO userInfoDTO) {
        return userInfoService.register(userInfoDTO);
    }

    @ApiOperation(value = "登录", notes = "登录")
    @PostMapping("login")
    public ResponseDTO login(@RequestBody UserInfoDTO userInfoDTO) throws Exception {
        return userInfoService.login(userInfoDTO);
    }

    @ApiOperation(value = "获取公钥", notes = "获取公钥")
    @GetMapping("getPublicKey")
    public ResponseDTO getPublicKey(@RequestParam("phone") String phone) throws Exception {
        return userInfoService.getPublicKey(phone);
    }

    @ApiOperation(value = "加密密码", notes = "加密密码")
    @GetMapping("getEncryptPassword")
    public ResponseDTO getEncryptPassword(@RequestParam("publicKey") String publicKey, @RequestParam("password") String password) throws Exception {
        return userInfoService.getEncryptPassword(publicKey, password);
    }

}
