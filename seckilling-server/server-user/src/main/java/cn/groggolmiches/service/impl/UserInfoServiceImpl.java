package cn.groggolmiches.service.impl;

import cn.groggolmiches.common.constant.UserRedisConstant;
import cn.groggolmiches.common.dto.ResponseDTO;
import cn.groggolmiches.common.dto.SecurityUserDTO;
import cn.groggolmiches.common.enums.UserEnums;
import cn.groggolmiches.common.exception.UserException;
import cn.groggolmiches.common.util.*;
import cn.groggolmiches.dto.UserInfoDTO;
import cn.groggolmiches.entity.UserInfo;
import cn.groggolmiches.mapper.UserInfoMapper;
import cn.groggolmiches.service.UserInfoService;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.nacos.common.util.Md5Utils;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * @description: 用户信息业务层实现类
 * @author: zhanhong zhang
 * @date: 2020/11/7 18:58
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public ResponseDTO searchUserInfo(Integer pageNum, Integer pageSize) {
        // 获取用户信息列表
        List<UserInfo> userInfos = userInfoMapper.selectAll();
        List<UserInfoDTO> userInfoDTOS = BeanCopyHelper.copyCollection(userInfos, UserInfoDTO.class);

        return ResponseDTO.success(PageHelper.page(userInfoDTOS, pageNum, pageSize));
    }

    @Override
    public ResponseDTO register(UserInfoDTO userInfoDTO) {
        // 检查参数
        NecessaryPropertiesCheckHelper.checkNotNull(userInfoDTO.getPhone(), userInfoDTO.getPassword());

        String phone = userInfoDTO.getPhone();
        String password = userInfoDTO.getPassword();

        // 检查用户名是否存在
        Example example = new Example(UserInfo.class);
        example.createCriteria().andEqualTo("phone", phone);
        UserInfo checkAccountIfExists = userInfoMapper.selectOneByExample(example);
        if (checkAccountIfExists != null) {
            ResponseDTO.response(UserEnums.ACCOUNT_EXISTS.getCode(), UserEnums.ACCOUNT_EXISTS.getMsg());
        }

        // MD5加密密码
        String md5Password = Md5Utils.getMD5(password.getBytes());
        userInfoDTO.setPassword(md5Password);

        // 构建用户信息实体类
        userInfoDTO.setId(SnowFlowerId.getId());
        userInfoDTO.setGmtCreate(new Date());
        userInfoDTO.setGmtModified(new Date());
        UserInfo userInfo = BeanCopyHelper.copyProperties(userInfoDTO, UserInfo.class);

        // 新增用户信息
        int result = userInfoMapper.insert(userInfo);
        if (result < 0) {
            throw new UserException(UserEnums.REGISTER_FAIL);
        }
        return ResponseDTO.success();
    }

    @Override
    public ResponseDTO login(UserInfoDTO userInfoDTO) throws Exception {
        byte[] encryptPassword = Base64.decodeBase64(userInfoDTO.getPassword());
        String phone = userInfoDTO.getPhone();

        // 密钥键key
        String key = UserRedisConstant.getUserPrivateKey(phone);
        if (!redisTemplate.hasKey(key)) {
            throw new UserException(UserEnums.ILLEGAL_REQUEST);
        }
        String privateKey = String.valueOf(redisTemplate.opsForValue().get(key));

        // 使用密钥解密密码
        byte[] decryptPassword = RSAUtil.decryptByPrivateKey(encryptPassword, Base64.decodeBase64(privateKey));

        // MD5加密密码
        String password = new String(decryptPassword);
        String md5Password = Md5Utils.getMD5(password.getBytes());

        // 查询数据库
        Example example = new Example(UserInfo.class);
        example.createCriteria().andEqualTo("phone", phone)
                .andEqualTo("password", md5Password);

        UserInfo userInfo = userInfoMapper.selectOneByExample(example);
        if (userInfo == null) {
            return ResponseDTO.response(UserEnums.WORONG_ACCOUNT_OR_PASSWORD.getCode(), UserEnums.WORONG_ACCOUNT_OR_PASSWORD.getMsg());
        }

        // 使用Base64对用户信息进行加密成token
        String token = loginAuth(userInfo);

        // 销毁redis中的密钥
        redisTemplate.delete(key);

        return ResponseDTO.success(token);
    }

    @Override
    public ResponseDTO getPublicKey(String account) throws Exception {
        // 初始化密钥对
        Map<String, Object> keyMap = RSAUtil.initKey();

        // 密钥键key
        String key = UserRedisConstant.getUserPrivateKey(account);
        byte[] publicKey = RSAUtil.getPublicKey(keyMap);
        byte[] privateKey = RSAUtil.getPrivateKey(keyMap);

        // 将密钥存入缓存
        redisTemplate.opsForValue().set(key, Base64.encodeBase64String(privateKey));

        return ResponseDTO.success(Base64.encodeBase64String(publicKey));
    }

    @Override
    public ResponseDTO getEncryptPassword(String publicKey, String password) throws Exception {
        byte[] encryptPassword = RSAUtil.encryptByPublicKey(password.getBytes(), Base64.decodeBase64(publicKey));
        return ResponseDTO.success(Base64.encodeBase64String(encryptPassword));
    }

    /**
     * 加入登录权限，并将登录信息加密成token后放入redis缓存，返回token
     *
     * @param userInfo：登录用户信息
     * @return token
     */
    private String loginAuth(UserInfo userInfo) {
        SecurityUserDTO securityUserDTO = new SecurityUserDTO();
        securityUserDTO.setId(userInfo.getId());
        securityUserDTO.setTimestamp(System.currentTimeMillis());

        // 默认为管理员权限
        Collection<SimpleGrantedAuthority> authorities = new LinkedList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_admin"));
        securityUserDTO.setAuthorities(authorities);

        // 将SecurityUserDTO转成json字符串后使用Base64加密成token
        String json = JSONObject.toJSONString(securityUserDTO);
        String token = Base64Utils.encodeToString(json.getBytes());

        // 将token加入缓存
        SecurityUtils.setUser(securityUserDTO.getId(), token);

        return token;
    }

}
