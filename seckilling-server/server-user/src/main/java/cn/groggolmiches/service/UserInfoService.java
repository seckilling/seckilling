package cn.groggolmiches.service;

import cn.groggolmiches.common.dto.ResponseDTO;
import cn.groggolmiches.dto.UserInfoDTO;

/**
 * @description: 用户信息业务层
 * @author: zhanhong zhang
 * @date: 2020/11/7 18:56
 */
public interface UserInfoService {

    /**
     * 分页查询用户信息
     * @param pageNum：页数
     * @param pageSize：页大小
     * @return 用户信息分页列表
     */
    ResponseDTO searchUserInfo(Integer pageNum,Integer pageSize);

    /**
     * 用户注册
     * @param userInfoDTO：用户信息DTO
     * @return 请求结果
     */
    ResponseDTO register(UserInfoDTO userInfoDTO);

    /**
     * 用户登录
     * @param userInfoDTO：用户信息DTO
     * @return 请求结果
     */
    ResponseDTO login(UserInfoDTO userInfoDTO) throws Exception;

    /**
     * 获取公钥
     * @param account：用户名
     * @return 返回公钥
     */
    ResponseDTO getPublicKey(String account) throws Exception;

    /**
     * 获取RSA加密后的密码（测试用，正常情况下由前端进行）
     * @param publicKey：公钥
     * @param password：未加密密码
     * @return 加密密码
     */
    ResponseDTO getEncryptPassword(String publicKey,String password) throws Exception;
}
