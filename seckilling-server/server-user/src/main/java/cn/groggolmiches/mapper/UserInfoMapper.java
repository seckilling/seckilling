package cn.groggolmiches.mapper;

import cn.groggolmiches.entity.UserInfo;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @description: 用户信息mapper
 * @author: zhanhong zhang
 * @date: 2020/11/7 18:47
 */
@Repository
public interface UserInfoMapper extends Mapper<UserInfo> {
}
